use std::env;
use std::error::Error;
use std::fs::File;
use std::fs;
use std::io::prelude::*;
use std::path::Path;

extern crate base64;

fn main() {
    let arguments: Vec<String> = env::args().collect::<Vec<String>>();
    if arguments.len() < 2 {
        panic!("Error getting path to read files from, you need to supply a command line argument specifying a directory")
    }

    let from_path: String = env::args().nth(1).unwrap();
    
    let paths = fs::read_dir(from_path).unwrap();

    for path in paths {
        let entry = &path.unwrap();
        if !entry.path().is_dir() {
            let mut file_from_handler = match File::open(&entry.path()) {
                Err(why) => panic!("Error: {}", why.description()), 
                Ok(file) => file,
            };
            let result: bool;
            let mut buffer: Vec<u8> = Vec::new();        
            let r = file_from_handler.read_to_end(&mut buffer);

            let mut data_to_write: Vec<u8> = Vec::new();
            let b64 = base64::encode(&buffer);

            let new_file: String = format!("{}{}", entry.path().display(), ".b64");
            let path_out = Path::new(&new_file);
            let mut file_out_handler = match File::create(&path_out) {
                Err(why) => panic!("couldn't create: {}", why.description()),
                Ok(file) => file,
            };

            match file_out_handler.write_all(&b64.as_bytes()) {
                Err(why) => panic!("couldn't write to: {}", why.description()),
                Ok(_) => println!("successfully wrote file"),
            }
        }

    }

    
    
    
}
